﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSpaceShip : MonoBehaviour
{
    [SerializeField] protected Bullet defaultBulletOne;
    [SerializeField] protected Bullet defaultBulletTwo;
    [SerializeField] protected Transform[] gunPosition;

    public int Hp { get; protected set; }
    public float Speed { get; protected set; }
    public Bullet Bullet { get; private set; }
    protected virtual void Init(int hp, float speed, Bullet bullet)
    {
        Hp = hp;
        Speed = speed;
        Bullet = bullet;
    }
    public virtual void Fire()
    {

    }
}
