﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D rb;
    public void Init(Vector2 direction)
    {
        Move(direction);
    }
    private void Awake()
    {
        Debug.Assert(rb != null, "rigidbody2D canot be null");
    }
    private void Move(Vector2 direction)
    {
        rb.velocity = direction * speed;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "ObjectDestroyer")
        {
            Destroy(gameObject);
            return;
        }
        var target = other.gameObject.GetComponent<IDamagable>();
        target?.TakeHit(damage);
        Destroy(gameObject);
    }
}
