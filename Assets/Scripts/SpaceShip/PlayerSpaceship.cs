﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Manager;
public class PlayerSpaceship : BaseSpaceShip,IDamagable
{
    public event Action OnExploded;
    [SerializeField] private HealthBar healthBar;
    private void Awake()
    {
        Debug.Assert(defaultBulletOne != null, "defaultBullet cannot be null");
        Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        Debug.Assert(healthBar != null, "healthBar cannot be null");
        healthBar.SetMaxHealth(GameManager.Instance.playerSpaceshipHp);
    }
    public void Init(int hp,float speed)
    {
        base.Init(hp, speed, defaultBulletOne);
    }
    public override void Fire()
    {
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerfire);
        base.Fire();
        var bulletOne = Instantiate(base.defaultBulletOne, gunPosition[0].position, Quaternion.identity);
        var bulletTwo = Instantiate(base.defaultBulletTwo, gunPosition[1].position, Quaternion.identity);
        var bulletThree = Instantiate(base.defaultBulletOne, gunPosition[2].position, Quaternion.identity);
        bulletOne.Init(Vector2.up);
        bulletTwo.Init(Vector2.up);
        bulletThree.Init(Vector2.up);
    }
    public void TakeHit(int damage)
    {
        Hp -= damage;
        healthBar.SetHealth(Hp);
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerhit);
        if (Hp > 0)
        {
            return;
        }
        Exploded();
    }
    public void Exploded()
    {
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerdeath);
        Debug.Assert(Hp <= 0, "Hp is more than zero");
        gameObject.SetActive(false);
        Destroy(gameObject);
        OnExploded?.Invoke();
    }
}
