﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Manager;
public class EnemySpaceship : BaseSpaceShip,IDamagable
{
    public event Action OnExploded;
    [SerializeField] private float enemyFireRate;
    [SerializeField] private float fireCounter ;
    [SerializeField] private HealthBar healthBar;
    private void Awake()
    {
        Debug.Assert(healthBar != null, "healthBar cannot be null");
        healthBar.SetMaxHealth(GameManager.Instance.enemySpaceshipHp);
    }
    public void Init(int hp,float speed)
    {
        base.Init(hp, speed, defaultBulletOne);
    }
    public override void Fire()
    {
        base.Fire();
        fireCounter += Time.deltaTime;
        healthBar.SetHealth(Hp);
        if (fireCounter >= enemyFireRate)
        {
            SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemyfire);
            var bullet = Instantiate(defaultBulletOne, gunPosition[0].position, Quaternion.identity);
            bullet.Init(Vector2.down);
            fireCounter = 0;
        }
    }
    public void TakeHit(int damage)
    {
        Hp -= damage;
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemyhit);
        if(Hp > 0)
        {
            return;
        }
        Exploded();
    }
    public void Exploded()
    {
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemydeath);
        Debug.Assert(Hp <= 0, "Hp is more than zero");
        gameObject.SetActive(false);
        Destroy(gameObject);
        OnExploded?.Invoke();
    }
    
}
