﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;

public class EnemyBossController : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private EnemyBossSpaceShip enemyBossSpaceShip;
    [SerializeField] private Rigidbody2D rb;
    private void Awake()
    {
        Debug.Assert(moveSpeed > 0, "moveSpeed has to be more than zero");
    }
    private void Update()
    {
        Move();
        enemyBossSpaceShip.Fire();
    }
    private void Move()
    {
        rb.velocity = new Vector2(moveSpeed * Time.fixedDeltaTime, 0);
    }
}
