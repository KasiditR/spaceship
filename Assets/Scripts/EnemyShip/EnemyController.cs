﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Manager;
namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float chasingTheresholdDistance;
        [SerializeField] private float moveSpeed;
        [SerializeField] private EnemySpaceship enemySpaceship;
        private void Awake()
        {
            Debug.Assert(chasingTheresholdDistance > 0, "chasingTheresholdDistance has to be more than zero");
            Debug.Assert(moveSpeed > 0, "moveSpeed has to be more than zero");
        }
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }
        private void MoveToPlayer()
        {
            var distanceToPlayer = Vector2.Distance(transform.position, GameManager.Instance.playerSpaceship.transform.position);
            if(distanceToPlayer > chasingTheresholdDistance)
            {
                return;
            }
            var directionToPlayer = (GameManager.Instance.playerSpaceship.transform.position - transform.position).normalized;
            transform.position += directionToPlayer * moveSpeed * Time.smoothDeltaTime;
        }
    }    
}

