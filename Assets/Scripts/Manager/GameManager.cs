﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button endgameButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform dialogStartgame;
        [SerializeField] private RectTransform dialogEndgame;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemyBossSpaceShip enemyBossSpaceShip;
        [Header("Player")]
        public int playerSpaceshipHp;
        public int playerSpaceshipMoveSpeed;
        [Header("Enemy")]
        public int enemySpaceshipHp;
        public int enemySpaceshipMoveSpeed;
        [Header("Boss")]
        public int enemyBossShipHp;
        public int enemyBossShipMoveSpeed;
        public PlayerSpaceship playerSpaceship;
        public event Action OnRestarted;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(endgameButton != null, "endgameButton canot be null");
            Debug.Assert(restartButton != null, "restartButton canot be null");
            Debug.Assert(dialogStartgame != null, "dialog cannot be null");
            Debug.Assert(dialogEndgame != null, "dialogEndgame cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(enemyBossSpaceShip != null, "enemyBossSpaceship cannot be null");

            SoundManager.Instance.PlayBGM();
            startButton.onClick.AddListener(OnStartButtononClicked);
        }
        private void OnStartButtononClicked()
        {
            dialogStartgame.gameObject.SetActive(false);
            StartGame();
        }
        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceShip();
            SpawnEnemySpaceShip();
        }
        private void SpawnPlayerSpaceShip()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }
        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }
        private void SpawnEnemySpaceShip()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }
        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.SetScore(1);
            SpawnEnemyBossSpaceShip();
            
        }
        private void SpawnEnemyBossSpaceShip()
        {
            var spaceship = Instantiate(enemyBossSpaceShip);
            spaceship.Init(enemyBossShipHp, enemyBossShipMoveSpeed);
            spaceship.OnExploded += OnEnemyBossSpaceshipExploded;
        }
        private void OnEnemyBossSpaceshipExploded()
        {
            ScoreManager.Instance.SetScore(1);
            EndGame();
        }
        private void Restart()
        {
            DestorRemainingShip();
            dialogEndgame.gameObject.SetActive(false);
            dialogStartgame.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }
        private void DestorRemainingShip()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
        private void EndGame()
        {
            if (enemySpaceship.Hp <= 0)
            {
                dialogEndgame.gameObject.SetActive(true);
                endgameButton.onClick.AddListener(OnApplicationQuit);
                restartButton.onClick.AddListener(Restart);
            }
        }
        private void OnApplicationQuit()
        {
            Application.Quit();
            Debug.Log("quit");
        }

    }
}

