﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private int playerScore;
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
            Debug.Assert(finalScoreText != null, "finalScoreText cannot null");
        }
        public void Init()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }
        public void SetScore(int score)
        {
            scoreText.text = $"Score:{score}";
            playerScore += score;
            finalScoreText.text = $"Player Score:{playerScore}";
        }
        private void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }
        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }

    }
}
